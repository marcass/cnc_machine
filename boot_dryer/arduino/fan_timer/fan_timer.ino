// constants won't change. They're used here to
// set pin numbers:
const int BUTTON_PIN = 5;    // the number of the pushbutton pin
const int RELAY_PIN = 12;
const int LED_PIN = 4; // the number of the LED pin
const int STATE_IDLE = 1;
const int STATE_DRYING = 2;

// Variables will change:
int state = 1;
int pressed;
int buttonState;             // the current reading from the input pin
int lastButtonState = LOW;   // the previous reading from the input pin

// the following variables are unsigned long's because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers

// timer
unsigned long start_timer;
unsigned long DRY_TIME = 3600000; //run period of 1hr
//unsigned long DRY_TIME = 30000; //test period od 30s

void setup() {
  Serial.begin (115200);
  pinMode(BUTTON_PIN, INPUT);
  pinMode(RELAY_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);

  // set initial LED state
  digitalWrite(RELAY_PIN, LOW);
  digitalWrite(LED_PIN, HIGH);
}

void proc_idle(int button){
  if (button == 1){
    start_timer = millis();
    digitalWrite(RELAY_PIN,HIGH);
    state = 2;
    Serial.println("Changed to drying");
    pressed = 0;
    delay(500);
  }else{
//    do nothing
    Serial.println("Idling.....");
  }
}

void proc_drying(int button){
  if (button == 1){
    digitalWrite(RELAY_PIN,LOW);
    state = 1;
    Serial.println("Changed to idle");
    pressed = 0;
    delay(500);
  }else{
//    see if timer kicks
    if (millis() - start_timer > DRY_TIME){
      digitalWrite(RELAY_PIN,LOW);
    state = 1;
    }
//    do nothing if in timer
    Serial.println("Drying....");
  }
}

void loop() {

  // read the state of the switch into a local variable:
  int reading = digitalRead(BUTTON_PIN);
//  Serial.println(reading);
  // check to see if you just pressed the button
  // (i.e. the input went from LOW to HIGH),  and you've waited
  // long enough since the last press to ignore any noise:

  // If the switch changed, due to noise or pressing:
  if (reading != HIGH) {
    // reset the debouncing timer
    pressed = 0;
    lastDebounceTime = millis();
  }
  if (reading == HIGH){
    if ((millis() - lastDebounceTime) > debounceDelay) {
      // whatever the reading is at, it's been there for longer
      // than the debounce delay, so take it as the actual current state:
      pressed = 1;
    }
  }
//  lastButtonState = reading;
  
  switch (state) {
    case STATE_IDLE:
      proc_idle(pressed);
      break;
    case STATE_DRYING:
      proc_drying(pressed);
      break;
  }
}

